<?php

namespace Drupal\search_overrides;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Utility class to relate entities to search_override entities.
 */
class SearchOverrideReferencer {
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Type of entity to reference.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The id of the referenced entity.
   *
   * @var int
   */
  protected $eid;

  /**
   * Search keyword.
   *
   * @var string
   */
  protected $keyword;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Retrieve search_overrides matching provided criteria.
   *
   * @param int $eid
   *   Id of the entity to look for.
   * @param string $entityTypeId
   *   Type of entity to search against.
   * @param array $options
   *   Optional array of parameters, including
   *   - query (string)
   *   - which - string, matching elnid or exnid.
   *
   * @return array
   *   An array of matching overrides.
   */
  public function findOverridesByEntity($eid, $entityTypeId = "node", array $options = []) {
    $overrideStorage = $this->entityTypeManager->getStorage('search_override');
    $elQuery = $overrideStorage->getQuery();
    if (isset($options['which'])) {
      // If specified, search in a particular column.
      if ($options['which'] == 'elnid') {
        $elQuery->condition('elnid', $eid);
      }
      elseif ($options['which'] == 'exnid') {
        $elQuery->condition('exnid', $eid);
      }
    }
    else {
      // Search for matches in either column.
      $or = $elQuery->orConditionGroup();
      $or->condition('elnid', $eid);
      $or->condition('exnid', $eid);
      $elQuery->condition($or);
    }
    if (isset($options['query'])) {
      $elQuery->condition('query', $options['query']);
    }

    $elevates = $elQuery->accessCheck(TRUE)->execute();
    return $elevates;
  }

  /**
   * Generate a table to display existing overrides for an entity.
   *
   * @param int $eid
   *   Id of the entity.
   * @param string $entityTypeId
   *   Type of entity.
   *
   * @return array
   *   Table render array.
   */
  public function entityOverridesTable($eid, $entityTypeId = "node") {
    $fields = ['elnid' => t("Promoted"), 'exnid' => t("Excluded")];
    $overrideStorage = $this->entityTypeManager->getStorage('search_override');
    /** @var \Drupal\Core\Entity\EntityListBuilder $list_builder */
    $list_builder = $this->entityTypeManager->getListBuilder('search_override');
    $rows = [];

    foreach ($fields as $field => $field_label) {
      $rows[] = [
        'query' => [
          'class' => ['search_override-table__heading'],
          'data' => new FormattableMarkup('<h4>@heading</h4>', ['@heading' => $field_label]),
          'colspan' => 2,
        ],
      ];
      // Now add rows for any existing values.
      $elQuery = $overrideStorage->getQuery();
      $elevates = $elQuery->condition($field, $eid)->accessCheck(TRUE)->execute();
      foreach ($elevates as $elevate_id) {
        $row = [];
        $elevate = $overrideStorage->load($elevate_id);
        $query_string = $elevate->get('query')->getString();
        $row['query'] = [
          'class' => ['search_override-table__query'],
          'data' => ['#plain_text' => $query_string],
          '#wrapper_attributes' => [
            '#prefix' => '<h4>',
            '#suffix' => '</h4>',
          ],
        ];
        $links = $list_builder->getOperations($elevate);
        unset($links['delete']);
        foreach ($links as $key => $link) {
          $links[$key]['attributes'] = [
            'class' => 'use-ajax',
            'data-dialog-options' => '{"width":500}',
            // @todo Make the specifics configurable in the settings.
            'data-dialog-type' => 'modal',
          ];
          if ($key == 'edit') {
            $links[$key]['title'] = $this->t('Edit / Reorder');
          }
        }
        $new_links['remove'] = [
          'title' => $this->t('Remove'),
          'weight' => 20,
          'url' => Url::fromRoute('search_overrides.override.remove.ajax', [
            'override' => $elevate_id,
            'field' => $field,
            'entity' => $eid,
          ]),
          'attributes' => [
            'class' => 'use-ajax',
            'data-dialog-type' => 'modal',
          ],
        ];
        $links = $new_links + $links;
        $row['operations'] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];
        $rows[] = $row;
      }
    }
    // Add the list to the vertical tabs section of the form.
    $header = [
      ['class' => ['search_overrides--table--query'], 'data' => t('Query')],
      ['class' => ['search_overrides--table--operations'], 'data' => t('Operations')],
    ];
    $table = [
      '#type' => 'table',
      '#name' => 'search_overrides_table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No overrides.'),
      '#attributes' => ['class' => ['search-overrides--table']],
      '#prefix' => '<div id="search-overrides--data">',
      '#suffix' => '</div>',
    ];
    return $table;

  }

}
