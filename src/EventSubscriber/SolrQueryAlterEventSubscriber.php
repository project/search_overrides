<?php

namespace Drupal\search_overrides\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\search_api_solr\Event\PreQueryEvent;
use Drupal\search_api_solr\Event\SearchApiSolrEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Alters the query where necessary to implement business logic.
 *
 * @package Drupal\search_overrides\EventSubscriber
 */
class SolrQueryAlterEventSubscriber implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Symfony RequestStack object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   */
  public function __construct(RequestStack $request_stack, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    $this->requestStack = $request_stack;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SearchApiSolrEvents::PRE_QUERY => 'preQuery',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preQuery(PreQueryEvent $event): void {
    $query = $event->getSearchApiQuery();
    $solarium_query = $event->getSolariumQuery();

    // Allow a method to review unmodified results.
    $ignore = $this->requestStack->getCurrentRequest()->query->get('ignore_overrides');
    if (!empty($ignore)) {
      // Show unmodified results.
      return;
    }

    // Get search terms.
    $query_keys = $query->getKeys();
    // Exit if no query was performed.
    if (!$query_keys) {
      return;
    }

    $conjunction = $query->getParseMode()->getConjunction();
    if (is_array($query_keys)) {
      unset($query_keys['#conjunction']);
    }
    else {
      $query_keys = explode(' ', $query->getKeys());
    }

    // Should we only match the entire search string?
    $match_entire_string = $this->configFactory->get('match_entire_string');

    // Add a search for a phrase match if more than one term.
    if (count($query_keys) > 1 && ($conjunction == 'AND')) {
      // Check if we should match only entire string or
      // entire string + each term individually.
      if ($match_entire_string) {
        $query_keys = [implode(' ', $query_keys)];
      }
      else {
        // Add full query to the individual terms.
        array_unshift($query_keys, implode(' ', $query_keys));
      }
    }

    // Try to find an override for this query. We don't use these results to
    // retrieve the overrides because they wouldn't be ordered.
    $overrideStorage = $this->entityTypeManager->getStorage('search_override');
    $elQuery = $overrideStorage->getQuery();
    $elevates = $elQuery->condition('query', $query_keys, 'IN')->accessCheck(TRUE)->execute();
    if (!$elevates) {
      // No record for this search query, so nothing to do.
      return;
    }

    // Initialize variables.
    $elSolrIds = [];
    $exSolrIds = [];
    $index = $query->getIndex();
    $current_lang = $this->languageManager->getCurrentLanguage()->getId();

    // Loop through search terms.
    foreach ($query_keys as $key) {
      $elQuery = $elQuery = $overrideStorage->getQuery();
      $elevates = $elQuery->condition('query', $key)
        ->accessCheck(TRUE)
        ->execute();
      if (!$elevates || !is_array($elevates)) {
        // No record or unexpected result for this search term, nothing to do.
        continue;
      }
      // Only load one entity per term, to reduce complexity.
      // @todo prevent duplicates from being stored.
      $elevate = $overrideStorage->load(array_shift($elevates));

      // Handle items to elevate.
      $elevateIds = $elevate->getElevatedIds();
      foreach ($elevateIds as $id) {
        $elSolrIds[] = search_overrides_make_solr_id($index, $id, $current_lang);
      }

      // Handle exclusions.
      $excludeIds = $elevate->getExcludedIds();
      foreach ($excludeIds as $id) {
        $exSolrIds[] = search_overrides_make_solr_id($index, $id, $current_lang);
      }
    }

    $solarium_query->addParam('elevateIds', implode(',', $elSolrIds));
    $solarium_query->addParam('excludeIds', implode(',', $exSolrIds));
  }

}
