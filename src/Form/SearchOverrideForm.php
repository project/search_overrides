<?php

namespace Drupal\search_overrides\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\search_overrides\SearchOverridesTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Form controller for Search elevate edit forms.
 *
 * @ingroup search_api_solr_elevate_exclude
 */
class SearchOverrideForm extends ContentEntityForm {

  use SearchOverridesTrait;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Symfony RequestStack object.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, RequestStack $request_stack) {
    $this->entityRepository = $entity_repository;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->time = $time;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('request_stack'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('search_overrides.settings');
    $content_match = $config->get('content_match');
    $index = FALSE;
    if ($content_match && $content_match == 'index') {
      $index = $config->get('search_index');
    }
    $this->processElement($form['elnid'], $form_state, $form, $index);
    $this->processElement($form['exnid'], $form_state, $form, $index);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  private function processElement(array &$element, FormStateInterface $form_state, array &$complete_form, $index = FALSE) {
    if ($element['#type'] == 'container') {
      $field_name = $element['widget']['#field_name'];
      $values = $form_state->getValue($field_name);
      foreach ($element['widget'] as $delta => $widget_part) {
        if (is_numeric($delta)) {
          $this->addAutocomplete($element['widget'][$delta]['value'], $form_state, $complete_form, $index);
        }
      }
    }
    else {
      $this->addAutocomplete($element, $form_state, $complete_form, $index);
    }
  }

  /**
   * Helper function to build out autocomplete inputs.
   */
  private function addAutocomplete(array &$element, FormStateInterface $form_state, array &$complete_form, $index = FALSE) {
    if ($index) {
      // Index was specified, so use to find matches.
      $element['#autocomplete_route_name'] = 'search_overrides.autocomplete';
      $element['#autocomplete_route_parameters'] = [
        'index_id' => $index,
        'count' => 10,
      ];
      if ($key = $element['#default_value']) {
        $entity = $this->getEntityFromValue($key);
        $element['#default_value'] = $entity->label() . ' (' . $key . ')';
      }

    }
    else {
      // Default to a standard, node autocomplete.
      $element['#type'] = 'entity_autocomplete';
      $element['#target_type'] = 'node';
      if ($key = $element['#default_value']) {
        $element['#default_value'] = $this->getEntityFromValue($key);
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $config = $this->config('search_overrides.settings');
    $content_match = $config->get('content_match');
    // No need for extra processing if using only nodes.
    if ($content_match == 'index') {
      $values['elnid'] = $form_state->getValue('elnid', []);
      $values['exnid'] = $form_state->getValue('exnid', []);
      // Parse out entity ids from returned values.
      foreach ($values as $field => $field_values) {
        foreach ($field_values as $index => $value_array) {
          if (is_numeric($index) && $value_array['value']) {
            $field_values[$index]['value'] = EntityAutocomplete::extractEntityIdFromAutocompleteInput($value_array['value']);
          }
        }
        $form_state->setValue($field, $field_values);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    // Determine if we're processing an AJAX form.
    $is_ajax = FALSE;
    $query = $this->requestStack->getCurrentRequest()->query;
    $destination = $query->get('destination');
    if ($destination && strpos($destination, 'ajax_form=1')) {
      $is_ajax = TRUE;
    }

    if (!$is_ajax) {
      switch ($status) {
        case SAVED_NEW:
          $this->messenger()->addMessage($this->t('Created the %label Search override.', [
            '%label' => $entity->label(),
          ]));
          break;

        default:
          $this->messenger()->addMessage($this->t('Saved the %label Search override.', [
            '%label' => $entity->label(),
          ]));
      }
      $form_state->setRedirect('entity.search_override.collection');
    }
    else {
      // Strip out URL parameters on the destination to avoid errors.
      $query->set('destination', strtok($destination, '?'));
    }

    return $status;
  }

}
